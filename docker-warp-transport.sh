export user
export password

# Разбираем аргументы для docker login
TEMP=`getopt --long -o "u:p:" "$@"`
eval set -- "$TEMP"
while true ; do
    case "$1" in
        -u )
            user=$2
            shift 2
        ;;
        -p )
            password=$2
            shift 2
        ;;
        *)
            break
        ;;
    esac 
done;

if readlink /proc/$$/exe | grep -q "dash"; then
	echo "This script needs to be run with bash, not sh"
	exit
fi

if [[ "$EUID" -ne 0 ]]; then
	echo "Sorry, you need to run this as root"
	exit
fi

if [[ -e /etc/debian_version ]]; then
	OS=debian
	GROUPNAME=nogroup
	RCLOCAL='/etc/rc.local'
elif [[ -e /etc/centos-release || -e /etc/redhat-release ]]; then
	OS=centos
	GROUPNAME=nobody
	RCLOCAL='/etc/rc.d/rc.local'
else
	echo "Looks like you aren't running this installer on Debian, Ubuntu or CentOS"
	exit
fi

# Обновляем базу пакетов
sudo apt-get update

# Добавляем утилиты APT
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

# Добавляем GPG ключ официального репозитария Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Добавляем репозиторий Docker в список источников пакетов утилиты APT
sudo apt-add-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Вновь обновляем информацию о доступных пакетах
sudo apt-get update

# Теперь устанавливаем Docker
sudo apt-get install -y docker-ce

# После завершения выполнения этой команды Docker должен быть установлен, демон запущен, и процесс должен запускаться при загрузке системы.
# Проверим, что процесс запущен:
# sudo systemctl status docker.service

# Авторизуемся в docker hub
docker login -u $user -p $password

# Останавливаем и удаляем контейнер, если он запущен
if [ "$(docker ps -q --filter ancestor=jillercr/warp-transport-service)" ]; then
	docker stop $(docker ps -a -q --filter ancestor=jillercr/warp-transport-service --format="{{.ID}}")
fi

if [ "$(docker ps -a -q --filter ancestor=jillercr/warp-transport-service)" ]; then
	docker rm $(docker stop $(docker ps -a -q --filter ancestor=jillercr/warp-transport-service --format="{{.ID}}"))
fi

# Пулим и запускаем контейнер
docker pull jillercr/warp-transport-service:latest
docker run --name warp-transport -d --publish 8776:5000 -e ASPNETCORE_URLS="http://+:5000" -e ASPNETCORE_ENVIRONMENT=Production --restart unless-stopped jillercr/warp-transport-service:latest

# To execure script run:
# wget -q -O deploy.sh https://bitbucket.org/jiller/docker-tools/raw/master/docker-warp-transport.sh
# chmod +x deploy.sh
# sudo -i ./deploy.sh -u UserName -p Password
